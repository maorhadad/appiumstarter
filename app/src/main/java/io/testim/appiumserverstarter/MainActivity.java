package io.testim.appiumserverstarter;

import android.content.ComponentName;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startTest(View view) {

        final ComponentName cn = new ComponentName("io.appium.uiautomator2.server.test",
                "android.support.test.runner.AndroidJUnitRunner");

        //io.appium.uiautomator2.server.test/android.support.test.runner.AndroidJUnitRunner
        Bundle arguments = new Bundle();
        arguments.putString("class", "io.appium.uiautomator2.server.test.AppiumUiAutomator2Server");
        startInstrumentation(cn, null, arguments);

    }

}
